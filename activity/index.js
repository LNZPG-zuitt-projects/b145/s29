let http = require('http');

http.createServer((request, response) => {

	// 2.a
	if(request.url == "/" && request.method == "GET"){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('Welcome to Booking System')

	};

	// 2.b
	if(request.url == "/profile" && request.method == "GET"){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('Welcome to your profile!')
	};

	// 2.c
	if(request.url == "/courses" && request.method == "GET"){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end("Here's our courses available")
	};

	// 2.d
	if(request.url == "/addcourse" && request.method == "POST"){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('Add a course to our resources')
	};

	// 2.e
	if(request.url == "/updatecourse" && request.method == "PUT"){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('Update a course to our resources')
	};

	// 2.f
	if(request.url == "/archivecourses" && request.method == "DELETE"){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('Archive courses to our resources')
	};
}).listen(4000);

console.log('Activity server running at port 4000');